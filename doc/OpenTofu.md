Terraform
=========

The openTofu backend are store on gitlab.

Get started
-----------

* A [SSH User](https://registry.terraform.io/providers/bpg/proxmox/latest/docs#ssh-user) on proxmox
* Creating an API token in the Proxmox
  ```bash
  sudo pveum user token add provisoning@pve terraform -expire 0 -privsep 0 -comment "Terraform token"
  ```
* Add in the `tofu` folder, a file `.local.env`
  ```bash
  cat <<EOF > ../tofu/.local.env
  export CI_PROJECT_ID="56351348"
  export CI_SERVER_HOST="gitlab.com"
  export CI_SERVER_PROTOCOL="https"
  export CI_API_V4_URL="${CI_SERVER_PROTOCOL}://${CI_SERVER_HOST}/api/v4"  
  export TF_HTTP_USERNAME="XXXXXXXXX"
  export TF_HTTP_PASSWORD="glpat-XXXXXXXXX"
  export PROXMOX_VE_API_TOKEN="provisoning@pve!terraform=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
  export PROXMOX_VE_SSH_USERNAME="root"
  EOF
  ```
