Proxmox
=======

Dependencies
------------

```bash
cd ../ansible/proxmox/plays/
direnv allow .
ansible-galaxy collection install -r requirements.yml --force
ansible-galaxy role install -r requirements.yml --force
```

Step 1: Prerequisites
---------------------

### inventory

Add / update pve node to [inventory](../ansible/proxmox/inventory/hosts.yml)

### Setup the ansible user

```bash
cd ../ansible/proxmox/plays/
direnv allow .
ansible-playbook playbook-pve-onboard.yml -i "<NODE_IP>," --user=root -k
```

### Patch Proxmox

```bash
cd ../ansible/proxmox/plays/
direnv allow .
ansible-playbook playbook-pve.yml --user=ansible --private-key ~/.ssh/id_ed25519
```

Step 2: Creating an API token in the Proxmox
--------------------------------------------

* On Node PVE:
  ```bash
  pveum role add Provisoning -privs "Datastore.AllocateTemplate Datastore.Allocate Datastore.AllocateSpace Datastore.Audit Pool.Allocate Sys.Audit Sys.Console Sys.Modify VM.Allocate VM.Audit VM.Clone VM.Config.CDROM VM.Config.Cloudinit VM.Config.CPU VM.Config.Disk VM.Config.HWType VM.Config.Memory VM.Config.Network VM.Config.Options VM.Console VM.Migrate VM.Monitor VM.PowerMgmt SDN.Use"
  pveum user add provisoning@pve --password <password>
  pveum aclmod / -user provisoning@pve -role Provisoning
  pveum user token add provisoning@pve ansible -expire 0 -privsep 0 -comment "Ansible token"
  ```
* Store it

Step 3: Ansible vault
---------------------

```bash
cd ../ansible/proxmox/plays/
direnv allow .
ansible-vault create ../inventory/group_vars/proxmoxs/vault.yml
```
```
pve_api_user: provisoning@pve
pve_api_token_id: ansible
pve_api_token_secret: XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
```

Step 4: Proxmox Resources
-------------------------

* Create PVE Storage `cloud-images`

![PVE Storage](img/pve_storage.png)

* run playbook

```bash
cd ../ansible/proxmox/plays/
direnv allow .
ansible-playbook playbook-pve-resources.yml --user=ansible --private-key ~/.ssh/id_ed25519
```  
