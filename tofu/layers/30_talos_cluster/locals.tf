locals {
  terraform_remote_state_proxmox_images = format("%s/%s-proxmox-images", var.ci_remote_state_base_url, var.state_name_prefix)
  proxmox = {
    endpoint  = "https://192.168.50.110:8006/"
    node_name = "pve1"
  }
  talos_proxmox_image_id = data.terraform_remote_state.gitlab_state_proxmox_images.outputs.talos_image_module.proxmox_image_id
  talos_version          = data.terraform_remote_state.gitlab_state_proxmox_images.outputs.talos_image_module.talos_version
}
