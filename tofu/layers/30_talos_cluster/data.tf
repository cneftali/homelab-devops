data "terraform_remote_state" "gitlab_state_proxmox_images" {
  backend = "http"
  config = {
    address = local.terraform_remote_state_proxmox_images
  }
}

data "talos_client_configuration" "talos_config" {
  cluster_name         = var.cluster.name
  client_configuration = talos_machine_secrets.machine_secrets.client_configuration
  nodes                = [var.worker.ipv4_address]
  endpoints            = [var.control_plane.ipv4_address]
}

data "talos_machine_configuration" "talos_control_plane" {
  talos_version    = local.talos_version
  cluster_name     = var.cluster.name
  cluster_endpoint = format("https://%s:6443", var.control_plane.ipv4_address)
  machine_type     = "controlplane"
  machine_secrets  = talos_machine_secrets.machine_secrets.machine_secrets
}

data "talos_machine_configuration" "talos_worker" {
  talos_version    = local.talos_version
  cluster_name     = var.cluster.name
  cluster_endpoint = format("https://%s:6443", var.control_plane.ipv4_address)
  machine_type     = "worker"
  machine_secrets  = talos_machine_secrets.machine_secrets.machine_secrets
}

data "talos_cluster_health" "health" {
  client_configuration = data.talos_client_configuration.talos_config.client_configuration
  control_plane_nodes  = [var.control_plane.ipv4_address]
  worker_nodes         = [var.worker.ipv4_address]
  endpoints            = data.talos_client_configuration.talos_config.endpoints
  timeouts = {
    read = "10m"
  }

  depends_on = [
    talos_machine_configuration_apply.talos_control_plane_config_apply,
    talos_machine_configuration_apply.talos_worker_config_apply
  ]
}
