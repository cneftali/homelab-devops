variable "ci_remote_state_base_url" {
  description = "Base url of gitlab url project state"
  type        = string
  nullable    = false
}

variable "state_name_prefix" {
  description = "The Prefix used to get terraform layer state"
  type        = string
  nullable    = false
}

variable "cluster" {
  description = "Cluster configuration"
  type = object({
    name    = string
    gateway = optional(string, "192.168.50.254")
  })
  default = {
    name = "talos-cluster"
  }
}

variable "control_plane" {
  description = "Control Plane configuration"
  type = object({
    vm_id        = optional(number, 150)
    host_node    = string
    ipv4_address = string
    cpu = optional(object({
      cores = optional(number, 4)
    }), {})
    memory = optional(object({
      dedicated = optional(number, 4096)
    }), {})
    disk = optional(object({
      datastore_id = optional(string, "local-zfs")
      size         = optional(number, 40)
    }), {})
  })
  default = {
    host_node    = "talos-cp-01"
    ipv4_address = "192.168.50.150"
  }
}

variable "worker" {
  description = "Worker configuration"
  type = object({
    vm_id        = optional(number, 151)
    host_node    = string
    ipv4_address = string
    cpu = optional(object({
      cores = optional(number, 2)
    }), {})
    memory = optional(object({
      dedicated = optional(number, 4096)
    }), {})
    disk = optional(object({
      datastore_id = optional(string, "local-zfs")
      size         = optional(number, 40)
    }), {})
  })
  default = {
    host_node    = "talos-worker-01"
    ipv4_address = "192.168.50.151"
  }
}
