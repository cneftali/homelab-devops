Layer Talos Cluster
===================

Description
-----------

This layer is creating a Talos Cluster on Proxmox

Generate Plan:
--------------

```bash
direnv allow .
gitlab-tofu.sh plan
```

Documentation
-------------

Terraform documentation is generated automatically using [pre-commit hooks](http://www.pre-commit.com/). Follow installation instructions [here](../../../README.md#get-started).

<!-- BEGINNING OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.9 |
| proxmox | 0.70.1 |
| talos | 0.7.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [proxmox_virtual_environment_vm.talos_control_plane](https://registry.terraform.io/providers/bpg/proxmox/0.70.1/docs/resources/virtual_environment_vm) | resource |
| [proxmox_virtual_environment_vm.talos_worker](https://registry.terraform.io/providers/bpg/proxmox/0.70.1/docs/resources/virtual_environment_vm) | resource |
| [talos_cluster_kubeconfig.talos_kubeconfig](https://registry.terraform.io/providers/siderolabs/talos/0.7.1/docs/resources/cluster_kubeconfig) | resource |
| [talos_machine_bootstrap.talos_bootstrap](https://registry.terraform.io/providers/siderolabs/talos/0.7.1/docs/resources/machine_bootstrap) | resource |
| [talos_machine_configuration_apply.talos_control_plane_config_apply](https://registry.terraform.io/providers/siderolabs/talos/0.7.1/docs/resources/machine_configuration_apply) | resource |
| [talos_machine_configuration_apply.talos_worker_config_apply](https://registry.terraform.io/providers/siderolabs/talos/0.7.1/docs/resources/machine_configuration_apply) | resource |
| [talos_machine_secrets.machine_secrets](https://registry.terraform.io/providers/siderolabs/talos/0.7.1/docs/resources/machine_secrets) | resource |
| [talos_client_configuration.talos_config](https://registry.terraform.io/providers/siderolabs/talos/0.7.1/docs/data-sources/client_configuration) | data source |
| [talos_cluster_health.health](https://registry.terraform.io/providers/siderolabs/talos/0.7.1/docs/data-sources/cluster_health) | data source |
| [talos_machine_configuration.talos_control_plane](https://registry.terraform.io/providers/siderolabs/talos/0.7.1/docs/data-sources/machine_configuration) | data source |
| [talos_machine_configuration.talos_worker](https://registry.terraform.io/providers/siderolabs/talos/0.7.1/docs/data-sources/machine_configuration) | data source |
| [terraform_remote_state.gitlab_state_proxmox_images](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| ci\_remote\_state\_base\_url | Base url of gitlab url project state | `string` | n/a | yes |
| state\_name\_prefix | The Prefix used to get terraform layer state | `string` | n/a | yes |
| cluster | Cluster configuration | ```object({ name = string gateway = optional(string, "192.168.50.254") })``` | ```{ "name": "talos-cluster" }``` | no |
| control\_plane | Control Plane configuration | ```object({ vm_id = optional(number, 150) host_node = string ipv4_address = string cpu = optional(object({ cores = optional(number, 4) }), {}) memory = optional(object({ dedicated = optional(number, 4096) }), {}) disk = optional(object({ datastore_id = optional(string, "local-zfs") size = optional(number, 40) }), {}) })``` | ```{ "host_node": "talos-cp-01", "ipv4_address": "192.168.50.150" }``` | no |
| worker | Worker configuration | ```object({ vm_id = optional(number, 151) host_node = string ipv4_address = string cpu = optional(object({ cores = optional(number, 2) }), {}) memory = optional(object({ dedicated = optional(number, 4096) }), {}) disk = optional(object({ datastore_id = optional(string, "local-zfs") size = optional(number, 40) }), {}) })``` | ```{ "host_node": "talos-worker-01", "ipv4_address": "192.168.50.151" }``` | no |

## Outputs

| Name | Description |
|------|-------------|
| talos\_config\_resource | n/a |
| talos\_control\_plane\_resource | n/a |
| talos\_kubeconfig\_resource | n/a |
| talos\_worker\_config\_resource | n/a |
<!-- END OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
