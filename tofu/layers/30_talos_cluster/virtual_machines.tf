resource "proxmox_virtual_environment_vm" "talos_control_plane" {
  name        = var.control_plane.host_node
  description = "Talos Control Plane"
  tags        = ["terraform-managed", "k8s", "control-plane"]
  node_name   = local.proxmox.node_name
  on_boot     = true
  vm_id       = var.control_plane.vm_id

  machine       = "q35"
  scsi_hardware = "virtio-scsi-single"
  bios          = "seabios"

  agent {
    enabled = true
    trim    = true
  }

  cpu {
    cores = var.control_plane.cpu.cores
    type  = "host"
  }

  memory {
    dedicated = var.control_plane.memory.dedicated
  }

  network_device {
    bridge = "vmbr0"
  }

  tpm_state {
    datastore_id = var.control_plane.disk.datastore_id
    version      = "v2.0"
  }

  disk {
    datastore_id = var.control_plane.disk.datastore_id
    interface    = "scsi0"
    iothread     = true
    cache        = "writethrough"
    discard      = "on"
    ssd          = true
    size         = var.control_plane.disk.size
    file_id      = local.talos_proxmox_image_id
    file_format  = "raw"
  }

  boot_order = ["scsi0"]

  operating_system {
    type = "l26"
  }

  initialization {
    datastore_id = var.control_plane.disk.datastore_id
    dns {
      servers = ["1.1.1.1", "1.0.0.1"]
    }
    ip_config {
      ipv4 {
        address = format("%s/24", var.control_plane.ipv4_address)
        gateway = var.cluster.gateway
      }
    }
  }
}

resource "proxmox_virtual_environment_vm" "talos_worker" {
  name        = var.worker.host_node
  description = "Talos Worker"
  tags        = ["terraform-managed", "k8s", "worker"]
  node_name   = local.proxmox.node_name
  on_boot     = true
  vm_id       = var.worker.vm_id

  machine       = "q35"
  scsi_hardware = "virtio-scsi-single"
  bios          = "seabios"

  agent {
    enabled = true
    trim    = true
  }

  cpu {
    cores = var.worker.cpu.cores
    type  = "host"
  }

  memory {
    dedicated = var.worker.memory.dedicated
  }

  network_device {
    bridge = "vmbr0"
  }

  tpm_state {
    datastore_id = var.worker.disk.datastore_id
    version      = "v2.0"
  }

  disk {
    datastore_id = var.worker.disk.datastore_id
    interface    = "scsi0"
    iothread     = true
    cache        = "writethrough"
    discard      = "on"
    ssd          = true
    size         = var.worker.disk.size
    file_id      = local.talos_proxmox_image_id
    file_format  = "raw"
  }

  boot_order = ["scsi0"]

  operating_system {
    type = "l26"
  }

  initialization {
    datastore_id = var.worker.disk.datastore_id
    dns {
      servers = ["1.1.1.1", "1.0.0.1"]
    }
    ip_config {
      ipv4 {
        address = format("%s/24", var.worker.ipv4_address)
        gateway = var.cluster.gateway
      }
    }
  }
}
