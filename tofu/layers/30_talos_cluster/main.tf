resource "talos_machine_secrets" "machine_secrets" {
  talos_version = local.talos_version
}

resource "talos_machine_configuration_apply" "talos_control_plane_config_apply" {
  node                        = var.control_plane.ipv4_address
  client_configuration        = talos_machine_secrets.machine_secrets.client_configuration
  machine_configuration_input = data.talos_machine_configuration.talos_control_plane.machine_configuration

  lifecycle {
    replace_triggered_by = [proxmox_virtual_environment_vm.talos_control_plane]
  }

  depends_on = [
    proxmox_virtual_environment_vm.talos_control_plane
  ]
}

resource "talos_machine_configuration_apply" "talos_worker_config_apply" {
  node                        = var.worker.ipv4_address
  client_configuration        = talos_machine_secrets.machine_secrets.client_configuration
  machine_configuration_input = data.talos_machine_configuration.talos_worker.machine_configuration

  lifecycle {
    replace_triggered_by = [proxmox_virtual_environment_vm.talos_worker]
  }

  depends_on = [
    proxmox_virtual_environment_vm.talos_worker
  ]
}

resource "talos_machine_bootstrap" "talos_bootstrap" {
  node                 = var.control_plane.ipv4_address
  endpoint             = var.control_plane.ipv4_address
  client_configuration = talos_machine_secrets.machine_secrets.client_configuration

  depends_on = [
    talos_machine_configuration_apply.talos_control_plane_config_apply
  ]
}

resource "talos_cluster_kubeconfig" "talos_kubeconfig" {
  node                 = var.control_plane.ipv4_address
  endpoint             = var.control_plane.ipv4_address
  client_configuration = talos_machine_secrets.machine_secrets.client_configuration

  timeouts = {
    read = "1m"
  }

  depends_on = [
    talos_machine_bootstrap.talos_bootstrap,
    data.talos_cluster_health.health
  ]
}
