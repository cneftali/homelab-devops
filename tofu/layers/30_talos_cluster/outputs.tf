output "talos_config_resource" {
  value     = data.talos_client_configuration.talos_config
  sensitive = true
}

output "talos_worker_config_resource" {
  value     = data.talos_machine_configuration.talos_worker
  sensitive = true
}

output "talos_control_plane_resource" {
  value     = data.talos_machine_configuration.talos_control_plane
  sensitive = true
}

output "talos_kubeconfig_resource" {
  value     = talos_cluster_kubeconfig.talos_kubeconfig
  sensitive = true
}
