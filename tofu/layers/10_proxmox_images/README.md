Layer Proxmox Images
====================

Description
-----------

This layer upload ISO to proxmox

Generate Plan:
--------------

```bash
direnv allow .
gitlab-tofu.sh plan
```

Documentation
-------------

Terraform documentation is generated automatically using [pre-commit hooks](http://www.pre-commit.com/). Follow installation instructions [here](../../../README.md#get-started).

<!-- BEGINNING OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.9 |
| proxmox | 0.70.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| talos\_image | ../../modules/talos-images | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| talos\_version | version of Talos OS | `string` | `"v1.9.3"` | no |

## Outputs

| Name | Description |
|------|-------------|
| talos\_image\_module | Module talos-images output |
<!-- END OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
