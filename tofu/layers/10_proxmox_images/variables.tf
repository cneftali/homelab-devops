variable "talos_version" {
  description = "version of Talos OS"
  type        = string
  default     = "v1.9.3"
}
