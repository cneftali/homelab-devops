provider "proxmox" {
  endpoint = local.proxmox.endpoint
  insecure = true
}
