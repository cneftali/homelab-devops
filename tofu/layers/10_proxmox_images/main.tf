module "talos_image" {
  source = "../../modules/talos-images"
  proxmox_download = {
    datastore_id = local.proxmox.iso_storage
    node_name    = local.proxmox.node_name
  }
  talos_version = var.talos_version
}
