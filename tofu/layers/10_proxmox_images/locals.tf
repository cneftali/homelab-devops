locals {
  proxmox = {
    endpoint    = "https://192.168.50.110:8006/"
    node_name   = "pve1"
    iso_storage = "local"
  }
}
