output "talos_version" {
  description = "The Talos OS image version"
  value       = var.talos_version
}

output "talos_schematic_id" {
  description = "The Talos schematic yaml representation to generate the image"
  value       = talos_image_factory_schematic.this.id
}

output "proxmox_image_id" {
  description = "The Proxmox image identifier"
  value       = proxmox_virtual_environment_download_file.this.id
}
