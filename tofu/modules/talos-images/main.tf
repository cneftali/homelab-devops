resource "talos_image_factory_schematic" "this" {
  schematic = yamlencode(
    {
      customization = {
        systemExtensions = {
          officialExtensions = data.talos_image_factory_extensions_versions.this.extensions_info[*].name
        }
      }
    }
  )
}

resource "proxmox_virtual_environment_download_file" "this" {
  content_type = "iso"
  datastore_id = var.proxmox_download.datastore_id
  node_name    = var.proxmox_download.node_name

  file_name = local.talos_file_name
  url       = data.talos_image_factory_urls.this.urls.iso
  overwrite = false
}
