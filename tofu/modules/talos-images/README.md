Module Proxmox Talos Images
===========================

This module upload to proxmox a talos images version to be used.

<!-- BEGINNING OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.9 |
| proxmox | >= 0.70 |
| talos | >= 0.7 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [proxmox_virtual_environment_download_file.this](https://registry.terraform.io/providers/bpg/proxmox/latest/docs/resources/virtual_environment_download_file) | resource |
| [talos_image_factory_schematic.this](https://registry.terraform.io/providers/siderolabs/talos/latest/docs/resources/image_factory_schematic) | resource |
| [talos_image_factory_extensions_versions.this](https://registry.terraform.io/providers/siderolabs/talos/latest/docs/data-sources/image_factory_extensions_versions) | data source |
| [talos_image_factory_urls.this](https://registry.terraform.io/providers/siderolabs/talos/latest/docs/data-sources/image_factory_urls) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| proxmox\_download | Proxmox download configuration | ```object({ datastore_id = string node_name = string })``` | n/a | yes |
| talos\_version | Talos image version | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| proxmox\_image\_id | The Proxmox image identifier |
| talos\_schematic\_id | The Talos schematic yaml representation to generate the image |
| talos\_version | The Talos OS image version |
<!-- END OF PRE-COMMIT-OPENTOFU DOCS HOOK -->

## Usage

In your `main.tf`, add the following code:

```hcl
module "my_module_name" {
  source = "../../modules/talos-images"

  proxmox_download = {
    datastore_id = "local"
    node_name    = "pve1"
  }

  talos_version = "v1.9.3"
}
```

Note:

- **my\_module\_name** is the name of the module. You can use any name you want.
