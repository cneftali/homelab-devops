variable "talos_version" {
  description = "Talos image version"
  type        = string
  nullable    = false
}

variable "proxmox_download" {
  description = "Proxmox download configuration"
  type = object({
    datastore_id = string
    node_name    = string
  })
  nullable = false
}
