My HomeLab
==========

Hardware - Virtualization
-------------------------

1. Minisforum NAB7(Core i7-12700H - 32 Go - 1 To DDR4)
2. [Proxmox ISO Installer](https://www.proxmox.com/en/downloads)

Binary requirements
-------------------

1. [asdf](https://asdf-vm.com/)
2. pip packages listed in [requirements.txt](requirements.txt)

Get started
-----------

1. Clone Project
    ```bash
    git clone https://gitlab.com/cneftali/homelab-infrastructure.git
    ```
2. For initialize pre-commit, execute in the repo folder:
    ```bash
    pre-commit install
   ```

Structure Directory
-------------------

```
project_dir
├── ansible
│   ├── proxmox
│   │   ├── inventory
│   │   │   └── hosts.yml
│   │   └── plays
│   │       ├── *
│   │       ├── ansible.cfg
│   │       ├── playbook-*.yml (Playbook)
│   │       └── requirements.yml
│   ├── roles
│   └── vms
└── terraform
    ├── layers
    │   └── *  
    └── modules
        └── *
```

Initialize Proxmox
------------------

[See](./doc/Initialize-Proxmox.md)

Proxmox and OpenTofu
--------------------

[See](./doc/OpenTofu.md)
