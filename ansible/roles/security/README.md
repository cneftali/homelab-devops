Ansible Role: Security
----------------------

This is a private ansible role to apply security parameters to servers

**Platforms Supported**:

| Platform | Versions |
|:---------|:---------|
| Debian   | bullseye |
| Debian   | bookworm |

⚠️ Requirements
---------------

Ansible >= 2.16.

Ansible role dependencies
-------------------------

None.

⚡ Usage
-------

### ✏️ Example Playbook

```yaml
- hosts: all
  roles:
    - role: security
```

⚙️ Role Variables
-----------------

### Default variables

Variables loaded from [defaults/main.yml](defaults/main.yml)

### Context variables

Variables loaded from [vars/main.yml](vars/main.yml)
