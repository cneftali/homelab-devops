Ansible Role Wifi
=================

A simple Ansible role to install and configure wpa_supplicant on Debian like systems.

**Platforms Supported**:

| Platform | Versions |
|:---------|:---------|
| Debian   | bookworm |

⚠️ Requirements
---------------

Ansible >= 2.16.

Ansible role dependencies
-------------------------

None.

⚡ Usage
-------

### ✏️ Example Playbook

```yaml
- hosts: all
  roles:
    - role: wifi
```

⚙️ Role Variables
-----------------

### Default variables

Variables loaded from [defaults/main.yml](defaults/main.yml)
