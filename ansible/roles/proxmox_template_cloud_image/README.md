Ansible Role: Proxmox Template Cloud Image
------------------------------------------

Create Proxmox VM Template from cloud image.

⚠️ Requirements
---------------

Ansible >= 2.15.

Ansible role dependencies
-------------------------

None.

⚡ Usage
-------

### ✏️ Example Playbook

```yaml
- hosts: all
  roles:
    - role: proxmox_template_cloud_image
```

⚙️ Role Variables
-----------------

### Default variables

Variables loaded from [defaults/main.yml](defaults/main.yml)

### Context variables

Variables loaded from [vars/main.yml](vars/main.yml)
